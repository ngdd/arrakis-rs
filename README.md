<h1 align="center">arrakis-rs</h1>

<p align="center">Arrakis Rust client library</p>

---

## Installation

``` toml
[dependencies]
arrakis = { git = "https://git.ligo.org/ngdd/arrakis-rs" }
```

## Features

* Query live and historical timeseries data
* Describe channel metadata
* Search for channels matching a set of conditions
* Publish timeseries data

## Quickstart

Note: This crate uses Tokio for its asynchronous runtime and therefore, all calls
need to run in Tokio's event loop. Some example usage is shown below.

### Additional Dependencies

To use the tokio macros in the examples below you will depend on tokio.

To stream data you will need to include the futures-util crate as well.

To access the array data returned you will need to depend on some arrow crates.
(This functionality is not yet shown in the examples).

```toml
[dependencies]
arrakis = { git = "https://git.ligo.org/ngdd/arrakis-rs" }
tokio = { version = "1", features = ["full"] }
futures-util = "0.3.29"
arrow-array = "49"
```

### Stream timeseries

##### 1. Live data

``` rust
use std::error::Error;
use futures_util::StreamExt;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];

    // retrieve live streaming data
    let mut stream = arrakis::stream(channels, None, None).await?;

    // process blocks as they become available
    while let Some(block) = stream.next().await {
        println!("{:?}", block);
    }
    Ok(())
}
```

##### 2. Historical data

``` rust
use std::error::Error;
use futures_util::StreamExt;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];
    let start = Some(1234500000);
    let end = Some(1234500100);

    // retrieve 100s of streaming data
    let mut stream = arrakis::stream(channels, start, end).await?;

    // process blocks as they become available
    while let Some(block) = stream.next().await {
        println!("{:?}", block);
    }
    Ok(())
}
```

### Fetch timeseries

TODO

### Describe metadata

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];

    // get metadata for channels requested
    let metadata = arrakis::describe(channels).await?;
    println!("metadata: {:?}", metadata);
    Ok(())
}
```

### Find channels

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    // channel pattern to match
    let pattern = Some(String::from("H1:CAL-*"));

    // find channels
    let channel_list = arrakis::find(pattern, None, None, None).await?;
    println!("channels: {:?}", channel_list);
    Ok(())
}
```

### Count channels

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    // channel pattern to match
    let pattern = Some(String::from("H1:CAL-*"));

    // count channels
    let count = arrakis::count(pattern, None, None, None).await?;
    println!("num channels: {:?}", count);
    Ok(())
}
```

### Publish timeseries

TODO
