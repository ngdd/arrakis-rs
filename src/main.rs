// Copyright (C) 2022, California Institute of Technology and contributors
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use futures::stream::StreamExt;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // iterate
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];
    let start = Some(1234500000);
    let end = Some(1234500100);

    let publisher = arrakis::register_publisher("DAQ", "X6", "PEM*").await?;

    // retrieve 100s of streaming data
    let mut stream = arrakis::stream(channels.clone(), start, end).await?;

    // process blocks as they become available
    while let Some(block) = stream.next().await {
        println!("{:?}", block);
    }

    // get metadata for channels requested
    let metadata = arrakis::describe(channels).await?;
    println!("metadata: {:?}", metadata);

    // channel pattern to match
    let pattern = Some(String::from("H1:CAL-*"));

    // find channels
    let channel_list = arrakis::find(pattern.clone(), None, None, None).await?;
    println!("channels: {:?}", channel_list);

    // count channels
    let count = arrakis::count(pattern, None, None, None).await?;
    println!("num channels: {:?}", count);

    Ok(())
}
