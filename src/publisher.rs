use std::collections::{HashSet, HashMap};
use std::error::Error as Error;
use std::fmt::Formatter;
use std::io::Write;
use std::iter::FromIterator;
use std::ops::Deref;
use std::sync::Arc;
use std::time::Duration;
use tokio::task::{JoinHandle, JoinSet};
use rayon::prelude::*;

use crate::data::{Channel, PublishChannel, BasicSeriesBlock, DataType, ChannelName, PartitionId, SeriesBackingStore};

use crate::iter::{BytesToI16Iterator, BytesToI32Iterator, BytesToI64Iterator, BytesToF32Iterator, BytesToF64Iterator, BytesToU32Iterator, FromU8Slice, OptionalDataToFlight, DataToFlight};
use crate::{errors, FlightClient, update_partitions, Request, PublishRequest};

use url::Url;
use uuid::Uuid;

use rdkafka::config::ClientConfig;
use rdkafka::producer::{BaseProducer, BaseRecord, DeliveryFuture, ProducerContext, ThreadedProducer, FutureProducer, FutureRecord, Producer};

use arrow::{
    array::{
        ArrayRef,
        Int16Array,
        Int16Builder,
        Int32Array,
        Int32Builder,
        Int64Array,
        Int64Builder,
        Float32Array,
        Float32Builder,
        Float64Array,
        Float64Builder,
        UInt32Array,
        UInt32Builder,
        StringArray,
        ListArray,
        ListBuilder,
        GenericListBuilder,
    },
    datatypes::{self, Field, Schema},
    ipc::writer::StreamWriter,
    record_batch::RecordBatch,
};
use arrow::array::{ArrowPrimitiveType, PrimitiveBuilder};
use arrow::ipc::Null;
use rdkafka::ClientContext;
use rdkafka::error::KafkaError;
use rdkafka::message::{DeliveryResult, ToBytes};
use tracing::{Level, event, info, warn, debug, span};

pub use crate::errors::ArrakisError;

// #[cfg(feature="future-producer-full")]
// type PublisherProducer = FutureProducer;
// #[cfg(feature="future-producer-full")]
// use future_producer_full::{create_producer, send_message, end_message_batch};
//
// #[cfg(feature="future-producer-quick")]
// type PublisherProducer = FutureProducer;
// #[cfg(feature="future-producer-quick")]
// use future_producer_quick::{create_producer, send_message, end_message_batch};
//
// #[cfg(feature="thread-producer-quick")]
// type PublisherProducer = ThreadedProducer<NullContext>;
// #[cfg(feature="thread-producer-quick")]
// use threaded_producer_quick::{create_producer, send_message, end_message_batch};

//#[cfg(feature="thread-producer-full")]
type PublisherProducer = threaded_producer_full::WrappedProducer;
//#[cfg(feature="thread-producer-full")]
use threaded_producer_full::{create_producer, send_message, end_message_batch};

struct NullContext {}

impl ClientContext for NullContext {}

impl ProducerContext for NullContext {
    type DeliveryOpaque = ();

    fn delivery(&self, delivery_result_: &DeliveryResult<'_>, delivery_opaque_: Self::DeliveryOpaque) {
    }
}

struct TransactionGuard<'a> {
    p: Option<&'a FutureProducer>,
    timeout: Duration,
}

impl<'input_lifetimes> TransactionGuard<'input_lifetimes> {
    fn new(p: &'input_lifetimes FutureProducer) -> Result<Self, ArrakisError> {
        p.begin_transaction()?;
        Ok(Self {
            p: Some(p),
            timeout: Duration::from_secs(15*60),
        })
    }

    fn commit(&mut self) -> Result<(), ArrakisError> {
        match self.p {
            Some(p) => {
                let start = std::time::SystemTime::now();
                let result = p.commit_transaction(self.timeout);
                let duration = start.elapsed();
                if result.is_err() {
                    match duration {
                        Ok(duration) => println!("commit transaction failed, taking {0}ms", duration.as_millis()),
                        _ => println!("could not time commit transaction"),
                    };
                }
                let _ignored = p.abort_transaction(self.timeout);
                self.p = None;
                result.map_err(|e| e.into())
            },
            None => Ok(())
        }
    }

    fn abort(&mut self, err: ArrakisError) -> ArrakisError {
        match self.p {
            Some(p) => {
                let _ = p.abort_transaction(self.timeout);
                self.p = None;
            },
            None => {},
        }
        err
    }

    fn set_timeout(&mut self, t: Duration) {
        self.timeout = t;
    }
}

impl Drop for TransactionGuard<'_> {
    fn drop(&mut self) {
        if let Some(p) = self.p {
            _ = p.abort_transaction(self.timeout);
        }
    }
}



pub(crate) struct PartitionMapping {
    pub channel_name: ChannelName,
    pub partition_id: PartitionId,
}

//type Partitions = HashMap<PartitionId, Vec<ChannelName>>;

#[derive(Default)]
struct Partitions {
    //by_part_id: HashMap<PartitionId, Vec<ChannelName>>,
    by_channel: HashMap<ChannelName, PartitionId>,
}

impl Partitions {
    fn new() -> Self {
        Self {
            by_channel: HashMap::new(),
        }
    }

    fn add_mapping(&mut self, mapping: PartitionMapping) {
        self.by_channel.insert(mapping.channel_name, mapping.partition_id);
    }

    fn bulk_add_mapping(&mut self, partition_id: &PartitionId, channels: &[ChannelName]) {
        channels.iter().for_each(|channel_name| {
            self.by_channel.insert(channel_name.clone(), partition_id.clone());
        })
    }

    fn lookup_channel(&self, channel_name: &ChannelName) -> Option<&PartitionId> {
        self.by_channel.get(channel_name)
    }
}

struct Logger {
    source: String,
    scope: String,
    path: String,
    entries: Vec<(std::time::SystemTime, String)>
}

impl Logger {
    fn append_logger(id: &str, source: &str, scope: &str) -> Self {
        let path = format!("/tmp/dkc_{0}_{1}.txt", id, scope);
        Self{ source: source.to_string(), scope: scope.to_string(), path, entries: Vec::new() }
    }

    fn log(&mut self, data: String) {
        self.entries.push((std::time::SystemTime::now(), data))
    }
}

impl Drop for Logger {
    fn drop(&mut self) {
        if self.entries.is_empty() {
            return;
        }
        let handle = std::fs::OpenOptions::new().create(true).write(true).append(true).open(self.path.as_str());
        if let Ok(mut f) = handle {
            let _ = write!(&mut f, "logs for {0} {1}\n", &self.source, &self.scope);
            let mut start: Option<std::time::SystemTime> = None;
            for entry in self.entries.as_slice() {
                let duration: u128 = match &start {
                    Some(t0) => entry.0.duration_since(t0.clone()).unwrap().as_millis(),
                    None => { start = Some(entry.0.clone()); 0},
                };
                let _ = write!(&mut f, "+{0}ms: {1}\n", duration, entry.1);
            }
            let _ = f.flush();
        }
    }
}

fn generate_kafka_publisher_config(initial_parameters: &HashMap<String, String>) -> Result<ClientConfig, ArrakisError> {
    let mut config = ClientConfig::new();
    config.set("message.max.bytes", "10000000")
        //.set("max.in.flight.requests.per.connection", "5")
        .set("max.in.flight.requests.per.connection", "5000")
        .set("acks", "0");
    //.set("enable.idempotence", "true");
    //.set("transactional.id", transaction_id);
    initial_parameters.iter().for_each(|(key, val)| { config.set(key, val); });
    if config.get("bootstrap.servers").is_none() {
        return Err(ArrakisError::BadUrl);
    }
    Ok(config)
}

pub struct BulkPublisherStatistics {
    pub channels_published: usize,
    pub missing_channels: usize,
    pub channels_that_need_partitioning: usize,
}

pub struct BulkPublisher {
    client: FlightClient,
    properties: HashMap<String, String>,
    partitions: HashMap<PartitionId, (Vec<ChannelName>, DataType)>,
    channels: HashMap<ChannelName, PublishChannel>,
    producer_id: String,
    source: String,
}

impl BulkPublisher {
    pub fn new(mut client: FlightClient, properties: &HashMap<String,String>, producer_id: String, source: String) -> Self {
        Self {
            client,
            properties: properties.clone(),
            partitions: HashMap::new(),
            channels: HashMap::new(),
            producer_id,
            source,
        }
    }

    pub async fn partition_channels(&mut self, input_channels: &HashSet<PublishChannel>) -> Result<(), ArrakisError> {
        let mut data_types: HashMap<ChannelName, DataType> = HashMap::new();
        let mut channels = HashMap::new();
        input_channels.iter().for_each(|channel| {
            let name = channel.channel_name();
            data_types.insert(name.clone(), channel.data_type.clone());
            channels.insert(name, channel.clone());

        });
        let mapping = update_partitions(&mut self.client, self.producer_id.as_str(), input_channels).await?;
        let mut by_partitions: HashMap<PartitionId, (Vec<ChannelName>, DataType)> = HashMap::new();
        mapping.into_iter().for_each(|m| {
            let key = m.partition_id;
            if let Some(mut group) = by_partitions.get_mut(&key) {
                group.0.push(m.channel_name);
            } else {
                let data_type = match data_types.get(&m.channel_name) {
                    Some(dt) => dt.clone(),
                    None => return,
                };
                let group = (vec![m.channel_name], data_type);
                by_partitions.insert(key, group);
            }
        });
        println!("Mapped {0} channels to {1} partitions", input_channels.len(), by_partitions.len());
        self.publish_metadata(channels.values()).await?;
        println!("Updated the server metadata");
        self.partitions = by_partitions;
        self.channels = channels;
        Ok(())
    }

    pub async fn allocate_publishers(&self) -> Result<Vec<(Publisher, Vec<String>)>, ArrakisError> {
        if self.partitions.is_empty() {
            return Ok(Vec::new());
        }
        let mut publishers: Vec<(Publisher, Vec<String>)> = Vec::new();
        const CHUNK_SIZE:usize = 120;
        let mut counter = 0;
        let mut partition_counter = 0usize;
        let mut channels = HashSet::new();
        let mut partitions = Partitions::new();
        let mut channel_names: Vec<String> = Vec::new();
        let mut partition_sizes = HashMap::new();
        let mut total_size = 0usize;
        self.partitions.iter().for_each(|(id, (channel_list, _))| {
            let mut parition_size = 0;
            for channel_name in channel_list {
                let chan = self.channels.get(channel_name).unwrap();
                parition_size += (chan.sample_rate as usize * chan.data_type.data_size()) + channel_name.0.len();
            }
            total_size += parition_size;
            partition_sizes.insert(id.clone(), parition_size);
        });
        let target_size = total_size / 32;
        let mut size_counter = 0;
        for partition in self.partitions.iter() {
            let partition_id = partition.0;
            let partition_channels = &partition.1.0;

            for name in partition_channels {
                let chan = self.channels.get(name).expect("missing channel definition");
                channels.insert(chan.clone());
                partitions.add_mapping(PartitionMapping{
                    channel_name: name.clone(),
                    partition_id: partition_id.clone(),
                });
                channel_names.push(name.0.clone());
            }

            partition_counter += 1;
            size_counter += partition_sizes.get(partition_id).unwrap();
            counter += 1;

            if partition_counter >= CHUNK_SIZE {
                //if size_counter >= target_size{
                let cur_channels = std::mem::take(&mut channels);
                let cur_partitions = std::mem::take(&mut partitions);
                let cur_channel_names = std::mem::take(&mut channel_names);
                let flight_client = crate::connect(None).await?;
                let new_publisher = Publisher::new_seeded(flight_client, &self.properties, self.producer_id.clone(), self.source.clone(), format!("seeded{0}", counter), cur_channels, cur_partitions)?;
                publishers.push((new_publisher, cur_channel_names));
                partition_counter = 0;
                size_counter = 0;
            }
        }
        if !channels.is_empty() {
            let flight_client = crate::connect(None).await?;
            let new_publisher = Publisher::new_seeded(flight_client, &self.properties, self.producer_id.clone(), self.source.clone(), format!("seeded{0}", counter), channels, partitions)?;
            publishers.push((new_publisher, channel_names));
        }
        Ok(publishers)
    }

    async fn publish_metadata<'a>(&self, channels: impl Iterator<Item = &'a PublishChannel>) -> Result<(), ArrakisError>
    {

        let topic = format!("arrakis-metadata-{}_{}", &self.source, &self.producer_id);
        let mut out = String::new();
        let mut first = true;
        let mut count: u64 = 0;
        let channels: Vec<&'a PublishChannel> = channels.collect();
        let kafka_config = generate_kafka_publisher_config(&self.properties)?;
        let p = create_producer(&kafka_config)?;
        //info!("channels collected");
        for channel in channels {
            let s = serde_json::to_string(channel)?;
            if !first {
                out.push_str("\n");
            }
            out.push_str(s.as_str());
            first = false;
            count += 1;

            if out.len() > 1024*1024 {
                if let Err(e) = send_message(&p, topic.as_str(), &out).await {
                    println!("Error queuing metadata message to {0} {1} bytes, {2:?}", &topic, out.len(), &e);
                    return Err(e);
                }
                out.clear();
            }
        }

        if !out.is_empty() {
            if let Err(e) = send_message(&p, topic.as_str(), &out).await {
                println!("Error queuing metadata message to {0} {1} bytes, {2:?}", &topic, out.len(), &e);
                return Err(e);
            }
        }
        //println!("sending metadata bundle of {0} bytes for worker {1}", out.len(), &self.source);
        //info!("metadata update blob built");


        //let start = std::time::Instant::now();

        if let Err(e) = end_message_batch(&p).await {
            println!("Error sending metadata message to {0} {1} bytes, {2:?}", &topic, out.len(), &e);
            return Err(e);
        }

        //let duration = std::time::Instant::now().checked_duration_since(start).unwrap_or_else(|_| std::time::Duration::from_secs(0));
        //info!("Sent metadata for {0}({3})({4}b) {1}ms {2}ms", &topic, iter_duration.as_millis(), duration.as_millis(), count, out.len());

        Ok(())
    }
}


pub struct Publisher {
    //p: Arc<FutureProducer>,
    p: Vec<Arc<PublisherProducer>>,
    p_config: ClientConfig,
    flight: FlightClient,
    channels: HashSet<PublishChannel>,
    producer_id: String,
    source: String,
    scope: String,
    partitions: Partitions,
    variable_channel_list: bool,
}

impl Publisher {
    fn new_seeded(flight: FlightClient, initial_parameters: &HashMap<String, String>, producer_id: String, source: String, label: String, channels: HashSet<PublishChannel>, partitions: Partitions) -> Result<Self, ArrakisError> {
        let transaction_id = Uuid::new_v4();

        let config= generate_kafka_publisher_config(initial_parameters)?;
        println!("kafka config: {:?}", &config);
        //let p:FutureProducer = config.create()?;
        let p = vec![
            //Arc::new(create_producer(&config)?),
            //Arc::new(config.create()?),
            //Arc::new(config.create()?),
            //Arc::new(config.create()?),
        ];
        //p.init_transactions(Duration::from_secs(2))?;

        let mut publisher = Self{
            //p: Arc::new(p),
            p,
            p_config: config,
            flight,
            channels,
            producer_id: producer_id.clone(),
            source,
            scope: label,
            partitions,
            variable_channel_list: false,
        };
        publisher.setup_kafka_producers(publisher.channels.len())?;
        Ok(publisher)
    }

    pub(crate) fn new(flight: FlightClient, initial_parameters: &HashMap<String, String>, producer_id: String, source: String, scope: String) -> Result<Self, ArrakisError> {
        let transaction_id = Uuid::new_v4();

        let config = generate_kafka_publisher_config(initial_parameters)?;
        println!("kafka config: {:?}", &config);
        //let p:FutureProducer = config.create()?;
        let p = vec![
            //Arc::new(create_producer(&config)?),
            //Arc::new(config.create()?),
            //Arc::new(config.create()?),
            //Arc::new(config.create()?),
        ];
        //p.init_transactions(Duration::from_secs(2))?;

        let mut publisher = Self{
            //p: Arc::new(p),
            p,
            p_config: config,
            flight,
            channels: HashSet::new(),
            producer_id: producer_id.clone(),
            source,
            scope,
            partitions: Partitions::new(),
            variable_channel_list: true,
        };
        Ok(publisher)
    }

    pub fn get_producer_id(&self) -> &str {
        self.producer_id.as_str()
    }

    pub fn get_source(&self) -> &str {
        self.source.as_str()
    }

    pub fn get_scope(&self) -> &str {
        self.scope.as_str()
    }

    fn setup_kafka_producers(&mut self, channel_count: usize) -> Result<(), ArrakisError> {
        let required_producers = (channel_count / 10000) + 1;
        self.p.truncate(required_producers);
        while self.p.len() < required_producers {
            self.p.push(Arc::new(create_producer(&self.p_config)?))
        }
        Ok(())
    }

    //#[tracing::instrument(skip(self, spawner, channels))]
    async fn publish_metadata<'a>(&self, spawner:&mut JoinSet<Result<(), ArrakisError>>, channels: impl Iterator<Item = &'a PublishChannel>) -> Result<(), ArrakisError>
    {
        //let start = std::time::SystemTime::now();
        let topic = format!("arrakis-metadata-{}_{}", &self.source, &self.producer_id);
        let mut out = String::new();
        let mut first = true;
        let mut count: u64 = 0;
        let channels: Vec<&'a PublishChannel> = channels.collect();
        //info!("channels collected");
        for channel in channels {
            let s = serde_json::to_string(channel)?;
            if !first {
                out.push_str("\n");
            }
            out.push_str(s.as_str());
            first = false;
            count += 1;
        }

        //println!("sending metadata bundle of {0} bytes for worker {1}", out.len(), &self.source);
        //info!("metadata update blob built");
        //let iter_duration = std::time::SystemTime::now().duration_since(start).unwrap();
        let p = Arc::clone(&self.p.first().unwrap());
        let abort_handle = spawner.spawn(async move {
            //let start = std::time::SystemTime::now();
            send_message(&p, topic.as_str(), &out).await?;
            end_message_batch(&p).await?;
            // let mut r = BaseRecord::to(topic.as_str()).payload(&out).key("");
            //
            // while let Err(ke) = p.send(r) {
            //     if let Some(code) = ke.0.rdkafka_error_code() {
            //         if code == rdkafka::types::RDKafkaErrorCode::QueueFull {
            //             tokio::time::sleep(std::time::Duration::from_micros(200)).await;
            //             r = ke.1;
            //             continue;
            //         }
            //         return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("kafka error code {}", code)));
            //     }
            //     return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("{:?}", &ke)));
            // }

            // let result = p.send_result(
            //     FutureRecord::to(topic.as_str()).payload(&out).key("")
            // );
            //let _ = was_sent(result).await?;
            //let duration = std::time::SystemTime::now().duration_since(start).unwrap();
            //info!("Sent metadata for {0}({3})({4}b) {1}ms {2}ms", &topic, iter_duration.as_millis(), duration.as_millis(), count, out.len());
            Ok(())
        });
        let _ = abort_handle;
        Ok(())
    }

    //#[tracing::instrument(skip(self, p, spawner, record_batch), level = Level::DEBUG)]
    // async fn publish_data(&self, p: Arc<PublisherProducer>, spawner:&mut JoinSet<Result<(), ArrakisError>>, partition_id: String, record_batch: RecordBatch) {
    //     let _ = spawner.spawn(async move {
    //         let topic = format!("arrakis-{0}", partition_id);
    //         let start = std::time::Instant::now();
    //         let buffer = serialize_record_batch(&record_batch);
    //         send_message(&p, topic.as_str(), &buffer).await?;
    //         let duration_ms = std::time::Instant::now().checked_duration_since(start).unwrap_or_else(|| std::time::Duration::from_secs(0)).as_millis();
    //         //info!("published {} - {}b - {}ms", &partition_id, buffer.len(), duration_ms);
    //         Ok(())
    //     });
    // }

    //#[tracing::instrument(skip(self, block))]
    pub async fn publish<T: SeriesBackingStore + Send + Sync + 'static>(&mut self, block: BasicSeriesBlock<T>) -> Result<(), ArrakisError>
    where
        <T as SeriesBackingStore>::Item: Send + Sync
    {
        //let publish_start = std::time::Instant::now();
        //println!("Entering publish, series block has {0} channels", block.metadata.len());
        if block.metadata.is_empty() {
            return Err(ArrakisError::EmptyBlock);
        }

        //let mut log = Logger::append_logger(self.producer_id.as_str(), self.source.as_str(), self.scope.as_str());

        let mut missing_channels = HashSet::new();
        if self.variable_channel_list {
            self.setup_kafka_producers(block.metadata.len())?;

            for (_, channel) in &block.metadata {
                if channel.source != self.source {
                    return Err(ArrakisError::InvalidChannelSource.into());
                }
                if !self.channels.contains(channel) {
                    missing_channels.insert(channel.clone());
                }
            }
        }

        let mut tasks = tokio::task::JoinSet::new();

        {
            //let mut transacation = TransactionGuard::new(&self.p)?;
            //log.log("starting transaction".to_string());

            if !missing_channels.is_empty() {

                let key_fn = |e: &PublishChannel| { e.name() };
                //println!("need to partition {0} channels", missing_channels.len());
                //info!("About to update partitions");
                let new_mappings = update_partitions(&mut self.flight, self.producer_id.as_str(), &missing_channels).await?;
                //info!("Paritions updated");
                let mut partitioned_channels = HashSet::new();

                //info!("converting missing channels to a vec");
                let mut missing_channels:Vec<PublishChannel> = missing_channels.into_iter().collect();
                missing_channels.sort_by_cached_key(key_fn);
                //info!("conversion and sort done");

                //println!("publishing metadata");
                self.publish_metadata(&mut tasks, new_mappings.iter().filter_map(|ch| {

                    let index = missing_channels.binary_search_by_key(&ch.channel_name.0, key_fn).ok()?;
                    //let pub_chan = missing_channels.iter().find(|e| { e.channel_name() == ch.channel_name })?;
                    let pub_chan = &missing_channels[index];
                    partitioned_channels.insert(pub_chan.clone());
                    Some(pub_chan)
                })).await?;


                let mut iter = new_mappings.into_iter();
                while let Some(e) = iter.next() {
                    self.partitions.add_mapping(e);
                }
                self.channels.extend(partitioned_channels);
            }

            let to_rb = ToOutputBuffer::new(block, &self.partitions);
            let partitions = to_rb.par_convert().await;

            //let mut send_jobs = JoinSet::new();
            let mut send_error: Option<ArrakisError> = None;
            let msg_count = partitions.len();
            for (partition_id, message) in partitions.into_iter() {
                let p = self.p[msg_count % self.p.len()].clone();
                //let _handle = send_jobs.spawn(async move {
                    let topic = format!("arrakis-{0}", partition_id);
                    match send_message(&p, topic.as_str(), &message).await {
                        Ok(_) => {},
                        Err(e) => {
                            println!("Error queuing kafka message {:?}", &e);
                            send_error = Some(e)
                        },
                    }
                //    Ok(())
                //});
            }



            // while let Some(result) = tasks.join_next().await {
            //     match result {
            //         Err(e) => {
            //             println!("JoinError happened {:?} - {1}", e, e.to_string());
            //             task_error = Some(ArrakisError::ErrorOnKafkaPublish(format!("Error on publish {}", e)));
            //         },
            //         Ok(val) => {
            //             match val {
            //                 Ok(_) => {},
            //                 Err(e) => {
            //                     println!("Underlying error on send {:?} - {1}", &e, e.to_string());
            //                     task_error = Some(e)
            //                 },
            //             };
            //         }
            //     };
            // }

            // if let Some(err) = task_error {
            //     return Err(err);
            // }
            //
            for p in self.p.iter_mut().take(msg_count) {
                if let Err(e) = end_message_batch(&*p).await {
                    println!("Error reported while sending a message {:?}", &e);
                    return Err(e);
                }
            }

            if let Some(e) = send_error {
                return Err(e);
            }

            // publish data
            //log.log("committing transaction".to_string());
            //let trans_span = span!(Level::INFO, "commiting transaction");
            //let _enter = trans_span.enter();
            //transacation.commit()?;
            //event!(Level::INFO, "transaction committed");
            //log.log("committed transaction".to_string());
        }
        //println!("publish for {} {} took {}ms", &self.source, &self.scope, std::time::Instant::now().checked_duration_since(publish_start).unwrap_or_else(|| std::time::Duration::from_secs(0)).as_millis());
        Ok(())
    }

    // fn transaction<F>(&self, f: F) -> Result<(), Box<dyn Error>>
    //     where F: FnOnce() -> Result<(), Box<dyn Error>>
    // {
    //     let mut transaction = TransactionGuard::new(&self.p)?;
    //     match f() {
    //         Ok(_) => transaction.commit(),
    //         Err(e) => Err(transaction.abort(e)),
    //     }
    // }
}

impl std::fmt::Debug for Publisher {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<Publisher {0} {1} {2}>", &self.producer_id, &self.source, &self.scope)
    }
}


fn serialize_record_batch(record_batch: &RecordBatch) -> Vec<u8> {
    let mut buffer = Vec::new();
    buffer.reserve(24*1024);
    let mut stream = StreamWriter::try_new(buffer, record_batch.schema().as_ref()).unwrap();
    stream.write(record_batch).unwrap();
    stream.into_inner().unwrap()
}

// Convert a Vec<Option<&[u8]>> into a Arrow ListArray, deriving the type from the
// iterator type I which converts from [u8] to the underlying data type
//
// This is a modified copy of the ListArray::from_iter_primitive to skip one level
// of Optionals.
fn to_list_array<'a, I, AT>(rows: Vec<Option<&'a [u8]>>) -> ListArray
where
    I: Iterator<Item=AT::Native> + FromU8Slice<'a>,
    AT: ArrowPrimitiveType
{
    let mut builder:GenericListBuilder<i32, PrimitiveBuilder<AT>> = GenericListBuilder::with_capacity(PrimitiveBuilder::<AT>::new(), rows.len());

    for row in rows {
        match row {
            Some(data) => {
                let it = I::new(data);
                for entry in it {
                    builder.values().append_value(entry);
                }
                builder.append(true);
            },
            None => builder.append(false),
        }
    }
    builder.finish()
}

fn to_record_batch<T: SeriesBackingStore>(partition_id: String, channel_list: Vec<String>, block: &BasicSeriesBlock<T>) -> Option<(String, RecordBatch)> {

    // FIXME: this should never fail
    let channel_data_type = {
        let first_channel = match channel_list.first() {
            Some(name) => name,
            None => return None,
        };
        block.metadata.get(first_channel).unwrap().data_type.clone()
    };

    let dt = match channel_data_type {
        DataType::Int16 => datatypes::DataType::Int16,
        DataType::Int32 => datatypes::DataType::Int32,
        DataType::Int64 => datatypes::DataType::Int64,
        DataType::Float32 => datatypes::DataType::Float32,
        DataType::Float64 => datatypes::DataType::Float64,
        DataType::UInt32 => datatypes::DataType::UInt32,
    };

    let schema = Arc::new(Schema::new(vec![
        Field::new("time", datatypes::DataType::Int64, false),
        Field::new("channel", datatypes::DataType::Utf8, false),
        Field::new("data", datatypes::DataType::List(Arc::new(Field::new("item", dt, true))), false),
    ]));
    let mut columns:Vec::<ArrayRef> = Vec::new();
    columns.reserve(channel_list.len());

    let data_type = channel_data_type.clone();
    let time_column: ArrayRef = Arc::new(Int64Array::from_value(block.time as i64, channel_list.len()));
    let raw_data: Vec<Option<&[u8]>> = channel_list.iter().map(|s| {
        match block.data.get(s) {
            Some(v) => Some(block.backing_store.data_item_to_slice(v)),
            None => None,
        }
    }).collect();
    let channel_column: ArrayRef = Arc::new(StringArray::from_iter_values(channel_list));

    let data_column = Arc::new(
        match data_type {
            DataType::Int16 => to_list_array::<BytesToI16Iterator, datatypes::Int16Type>(raw_data),
            DataType::Int32 => to_list_array::<BytesToI32Iterator, datatypes::Int32Type>(raw_data),
            DataType::Int64 => to_list_array::<BytesToI64Iterator, datatypes::Int64Type>(raw_data),
            DataType::Float32 => to_list_array::<BytesToF32Iterator, datatypes::Float32Type>(raw_data),
            DataType::Float64 => to_list_array::<BytesToF64Iterator, datatypes::Float64Type>(raw_data),
            DataType::UInt32 => to_list_array::<BytesToU32Iterator, datatypes::UInt32Type>(raw_data),
        });

    Some((partition_id, RecordBatch::try_new(schema, vec![time_column, channel_column, data_column]).expect("Could not build Array RecordBatch")))
}

struct ToOutputBuffer<T>
where
    T: SeriesBackingStore + Send,
    <T as SeriesBackingStore>::Item: Send
{
    block: Arc<BasicSeriesBlock<T>>,
    channels_by_part: HashMap<String, Vec<String>>,
}

impl<T: SeriesBackingStore + Send + Sync + 'static> ToOutputBuffer<T>
where
    <T as SeriesBackingStore>::Item: Send
{
    fn new(block: BasicSeriesBlock<T>, partition_map: &Partitions) -> Self {
        let mut channels_by_part: HashMap<String, Vec<String>> = HashMap::new();
        block.data.keys().for_each(|name| {
            if let Some(partition_id) = partition_map.lookup_channel(&ChannelName(name.clone())) {
                if let Some(ref mut channel_list) = channels_by_part.get_mut(&partition_id.0) {
                    channel_list.push(name.clone());
                } else {
                    channels_by_part.insert(partition_id.0.clone(), vec![name.clone()]);
                }
            }
        });
        Self{
            block: Arc::new(block),
            channels_by_part,
        }
    }

    async fn par_convert(self) -> Vec<(String, Vec<u8>)>
    where
        <T as SeriesBackingStore>::Item: Send + Sync
    {
        let mut results = Vec::new();
        let mut tasks = JoinSet::<Option<(String, RecordBatch)>>::new();
        for (partition_id, channel_list) in self.channels_by_part.into_iter() {
            if channel_list.is_empty() {
                continue;
            }
            let block = Arc::clone(&self.block);
            tasks.spawn(async move {
                to_record_batch(partition_id, channel_list, &*block)
            });
        }
        while let Some(task_result) = tasks.join_next().await {
            match task_result {
                Ok(part_info) => if let Some(part_info) = part_info {
                    results.push((part_info.0, serialize_record_batch(&part_info.1)));
                },
                Err(_) => {},
            }
        }
        results
    }

}

impl<T: SeriesBackingStore + Send + Sync + 'static> Iterator for ToOutputBuffer<T>
where
    <T as SeriesBackingStore>::Item: Send + Sync
{
    type Item = (String, Vec<u8>);

    //#[tracing::instrument(skip(self), level = Level::DEBUG)]
    fn next(&mut self) -> Option<Self::Item> {
        let mut partition_id = self.channels_by_part.keys().next()?.clone();
        let mut channel_list = self.channels_by_part.remove(partition_id.as_str())?;
        while channel_list.is_empty() {
            partition_id = self.channels_by_part.keys().next()?.clone();
            channel_list = self.channels_by_part.remove(partition_id.as_str())?;
        }
        match to_record_batch(partition_id, channel_list, &*self.block) {
            Some(val) => Some((val.0, serialize_record_batch(&val.1))),
            None => None,
        }
    }
}

fn host_and_port(url: &Url) -> Result<String, Box<dyn Error>> {
    let host = url.host_str().ok_or_else(|| Box::new(ArrakisError::BadUrl))?;
    match url.port() {
        Some(p) => Ok(format!("{0}:{1}", host, p)),
        None => Ok(host.to_string()),
    }
}

mod future_producer_full {
    use super::*;

    pub(crate) fn create_producer(config: &ClientConfig) -> Result<FutureProducer, ArrakisError> {
        Ok(config.create()?)
    }

    async fn was_sent<'a, K, P>(result: Result<DeliveryFuture, (KafkaError, FutureRecord<'a, K, P>)>) -> Result<(i32, i64), ArrakisError>
    where
        K: ToBytes + ?Sized,
        P: ToBytes + ?Sized,
    {
        let result = match result {
            Ok(delivery_future) => delivery_future.await,
            Err((e, _)) => return Err(e.into()),
        };
        let result = match result {
            Ok(owned_delivery_result) => owned_delivery_result,
            Err(e) => return Err(e.into()),
        };
        match result {
            Ok((val1, val2)) => Ok((val1, val2)),
            Err((e, _)) => Err(e.into()),
        }
    }

    pub(crate) async fn send_message<B: ToBytes + ?Sized>(p: &FutureProducer, topic: &str, buffer: &B) -> Result<(), ArrakisError>{
        let result = p.send_result(
            FutureRecord::to(topic).payload(buffer).key("")
        );
        let _ = was_sent(result).await?;
        Ok(())
    }

    pub (crate) async fn end_message_batch(p: &FutureProducer) -> Result<(), ArrakisError> {
        Ok(())
    }
}

mod future_producer_quick {
    use super::*;

    pub(crate) fn create_producer(config: &ClientConfig) -> Result<FutureProducer, ArrakisError> {
        Ok(config.create()?)
    }

    pub(crate) async fn was_sent<'a, K, P>(result: Result<DeliveryFuture, (KafkaError, FutureRecord<'a, K, P>)>) -> Result<(i32, i64), ArrakisError>
    where
        K: ToBytes + ?Sized,
        P: ToBytes + ?Sized,
    {
        let result = match result {
            Ok(delivery_future) => delivery_future.await,
            Err((e, _)) => return Err(e.into()),
        };
        Ok((0, 0))
    }

    pub(crate) async fn send_message<B: ToBytes + ?Sized>(p: &FutureProducer, topic: &str, buffer:&B) -> Result<(), ArrakisError>{
        let result = p.send_result(
            FutureRecord::to(topic).payload(buffer).key("")
        );
        let _ = was_sent(result).await?;
        Ok(())
    }

    pub (crate) async fn end_message_batch(p: &FutureProducer) -> Result<(), ArrakisError> {
        Ok(())
    }
}

mod threaded_producer_quick {
    use super::*;

    pub(crate) fn create_producer(config: &ClientConfig) -> Result<ThreadedProducer<NullContext>, ArrakisError> {
        Ok(config.create_with_context(NullContext{})?)
    }
    pub(crate) async fn send_message<B: ToBytes + ?Sized>(p: &ThreadedProducer<NullContext>, topic: &str, buffer: &B) -> Result<(), ArrakisError>{
        let mut r = BaseRecord::to(topic).payload(buffer).key("");

        while let Err(ke) = p.send(r) {
            if let Some(code) = ke.0.rdkafka_error_code() {
                if code == rdkafka::types::RDKafkaErrorCode::QueueFull {
                    tokio::time::sleep(std::time::Duration::from_micros(200)).await;
                    r = ke.1;
                    continue;
                }
                return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("kafka error code {}", code)));
            }
            return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("{:?}", &ke.0)));
        }
        Ok(())
    }

    pub (crate) async fn end_message_batch(p: &ThreadedProducer<NullContext>) -> Result<(), ArrakisError> {
        Ok(())
    }
}


mod threaded_producer_full {
    use tokio::sync::mpsc::{unbounded_channel, UnboundedSender, UnboundedReceiver};
    use super::*;

    enum PublishStatus {
        Publishing,
        Published,
        Error(ArrakisError),
        EndOfBlock,
    }

    pub struct FullProducerContext {
        cb_channel: UnboundedSender<PublishStatus>,
    }

    impl FullProducerContext {
        fn new(cb_channel: UnboundedSender<PublishStatus>) -> Self {
            Self{ cb_channel }
        }
    }

    impl ClientContext for FullProducerContext {}

    impl ProducerContext for FullProducerContext {
        type DeliveryOpaque = ();

        fn delivery(&self, delivery_result: &DeliveryResult<'_>, _delivery_opaque: Self::DeliveryOpaque) {
            let msg = match delivery_result {
                Ok(_) => PublishStatus::Published,
                Err((e, _)) => {
                    let error_code = match e.rdkafka_error_code() {
                        Some(code) => format!("error code {:?}", code),
                        None => format!("no error code"),
                    };
                    println!("Delivery callback reported error. {:?} - {1} - code {2}", &e, e.to_string(), error_code);
                    PublishStatus::Error(e.clone().into())
                },
            };
            self.cb_channel.send(msg).unwrap();
        }
    }

    pub(crate) struct WrappedProducer {
        p: ThreadedProducer<FullProducerContext>,
        sender: UnboundedSender<PublishStatus>,
        receiver: Arc<tokio::sync::Mutex<UnboundedReceiver<PublishStatus>>>,
    }

    pub(crate) fn create_producer(config: &ClientConfig) -> Result<WrappedProducer, ArrakisError> {
        let (s, r) = unbounded_channel();
        let ctx = FullProducerContext::new(s.clone());
        let p = config.create_with_context(ctx)?;
        Ok(WrappedProducer{
            p,
            sender: s,
            receiver: Arc::new(tokio::sync::Mutex::new(r)),
        })
    }
    pub(crate) async fn send_message<B: ToBytes + ?Sized>(p: &WrappedProducer, topic: &str, buffer: &B) -> Result<(), ArrakisError>{
        let mut r = BaseRecord::to(topic).payload(buffer).key("");

        while let Err(ke) = p.p.send(r) {
            if let Some(code) = ke.0.rdkafka_error_code() {
                if code == rdkafka::types::RDKafkaErrorCode::QueueFull {
                    tokio::time::sleep(std::time::Duration::from_micros(200)).await;
                    r = ke.1;
                    continue;
                }
                return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("kafka error code {}", code)));
            }
            return Err(errors::ArrakisError::ErrorOnKafkaPublish(format!("{:?}", &ke.0)));
        }
        // at this point we have enqueued something so we must mark that
        p.sender.send(PublishStatus::Publishing).unwrap();
        Ok(())
    }

    pub (crate) async fn end_message_batch(p: &WrappedProducer) -> Result<(), ArrakisError> {
        p.sender.send(PublishStatus::EndOfBlock).unwrap();
        let mut done = false;
        let mut remaining_results = 0;
        let mut receiver = p.receiver.lock().await;
        let mut last_error = None;
        while !(done && remaining_results == 0) {
            let msg = match receiver.recv().await {
                Some(m) => m,
                None => break,
            };
            match msg {
                PublishStatus::Publishing => {
                    if done {
                        panic!("Received a Publishing marker after none were possible");
                    }
                    remaining_results += 1;
                },
                PublishStatus::Published => remaining_results -= 1,
                PublishStatus::Error(e) => {
                    remaining_results -= 1;
                    last_error = Some(e);
                },
                PublishStatus::EndOfBlock => {
                    if done {
                        panic!("Multiple end of block markers found");
                    }
                    done = true
                },
            };
        }
        if let Some(e) = last_error {
            return Err(e);
        }
        Ok(())
    }
}
