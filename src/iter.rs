use std::convert::TryInto;
use std::iter::{ IntoIterator, Iterator };
use std::marker::PhantomData;

pub trait FromU8Slice<'a> {
    fn new(input: &'a [u8]) -> Self;
}

pub struct BytesToI16Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToI16Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToI16Iterator<'a>
{
    type Item = i16;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<i16>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<i16>());
        Some(i16::from_ne_bytes(cur.try_into().unwrap()))
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<i16>(), None)
    }
}

pub struct BytesToI32Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToI32Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToI32Iterator<'a>
{
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<i32>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<i32>());
        Some(i32::from_ne_bytes(cur.try_into().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<i32>(), None)
    }
}

pub struct BytesToI64Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToI64Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToI64Iterator<'a>
{
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<i64>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<i64>());
        Some(i64::from_ne_bytes(cur.try_into().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<i64>(), None)
    }
}

pub struct BytesToF32Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToF32Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToF32Iterator<'a>
{
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<f32>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<f32>());
        Some(f32::from_ne_bytes(cur.try_into().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<f32>(), None)
    }
}


pub struct BytesToF64Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToF64Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToF64Iterator<'a>
{
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<f64>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<f64>());
        Some(f64::from_ne_bytes(cur.try_into().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<f64>(), None)
    }
}

pub struct BytesToU32Iterator<'a>
{
    buffer: &'a [u8],
}

impl<'a> FromU8Slice<'a> for BytesToU32Iterator<'a>
{
    fn new(buffer: &'a [u8]) -> Self {
        Self{ buffer }
    }
}

impl<'a> Iterator for BytesToU32Iterator<'a>
{
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.buffer.len() < std::mem::size_of::<u32>() {
            return None;
        }
        let cur;
        (cur, self.buffer) = self.buffer.split_at(std::mem::size_of::<u32>());
        Some(u32::from_ne_bytes(cur.try_into().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.buffer.len() / std::mem::size_of::<u32>(), None)
    }
}



// The ProxyDataIterator is used to wrap a BytesTo...Iterator
// (of specific type T) and map the return time to Option<T::Item>
//
// This is used to provide a semi-generic conversion from &[u8] to
// Option<T> for Arrow ListArrays.
pub struct ProxyDataIterator<'a, T> {
    iter: T,
    type_marker: &'a PhantomData<T>,
}

impl<'a, T> ProxyDataIterator<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    pub fn new(data: &'a [u8]) -> Self {
        let iter = T::new(data);
        Self{iter, type_marker: &PhantomData}
    }
}

impl<'a, T> Iterator for ProxyDataIterator<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    type Item = Option<T::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some(v) => Some(Some(v)),
            None => None,
        }
    }
}

// The ProxyDataRowIterator is used to take a
// list of PublishChannelAndData and return a
// ProxyDataIterator for the data in each
// channel.
//
// The underlying data type is encoded in the
// T parameter which is the BytesTo...Iterator
// type to do the actual final conversion.
pub struct ProxyDataRowIterator<'a, T> {
    rows: &'a [&'a [u8]],
    type_marker: PhantomData<T>,
}

impl <'a, T: 'a> crate::iter::ProxyDataRowIterator<'a, T> {
    fn new(rows: &'a [&'a [u8]]) -> Self {
        Self{ rows, type_marker: PhantomData }
    }
}

impl <'a, T: 'a> Iterator for crate::iter::ProxyDataRowIterator<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    type Item = Option<ProxyDataIterator<'a, T>>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.rows.first() {
            Some(data) => {
                self.rows = &self.rows[1..];
                Some(Some(ProxyDataIterator::<T>::new(*data)))
            },
            None => None,
        }
    }
}

// The ProxyDataRowIterator is used to take a
// list of PublishChannelAndData and return a
// ProxyDataIterator for the data in each
// channel.
//
// The underlying data type is encoded in the
// T parameter which is the BytesTo...Iterator
// type to do the actual final conversion.
pub struct ProxyOptionalDataRowIterator<'a, T> {
    rows: &'a [Option<&'a [u8]>],
    type_marker: PhantomData<T>,
}

impl <'a, T: 'a> ProxyOptionalDataRowIterator<'a, T> {
    fn new(rows: &'a [Option<&'a [u8]>]) -> Self {
        Self{ rows, type_marker: PhantomData }
    }
}

impl <'a, T: 'a> Iterator for ProxyOptionalDataRowIterator<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    type Item = Option<ProxyDataIterator<'a, T>>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.rows.first() {
            Some(data) => {
                self.rows = &self.rows[1..];
                match data {
                    Some(row) => Some(Some(ProxyDataIterator::<T>::new(*row))),
                    None => Some(None),
                }
            },
            None => None,
        }
    }
}


// PublishChannelAndDataToFlight is a New Type that is used to
// provide a IntoIter for use with a list of PublishChannelAndData
// suitable for building a ListArray of the data.
pub struct DataToFlight<'a, T>(&'a [&'a [u8]], PhantomData<T>);

impl<'a, T: 'a> DataToFlight<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    pub fn new(data: &'a[&'a [u8]]) -> Self {
        Self(data, PhantomData)
    }
}

impl<'a, T: 'a> IntoIterator for DataToFlight<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    type Item = Option<ProxyDataIterator<'a, T>>;
    type IntoIter = ProxyDataRowIterator<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        ProxyDataRowIterator::new(self.0)
    }
}


// OptionalDataToFlight is a New Type that is used to
// provide a IntoIter for use with a list of Option<&[u8]>
// suitable for building a ListArray of the data.
pub struct OptionalDataToFlight<'a, T>(&'a [Option<&'a [u8]>], PhantomData<T>);

impl<'a, T: 'a> crate::iter::OptionalDataToFlight<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    pub fn new(data: &'a[Option<&'a [u8]>]) -> Self {
        Self(data, PhantomData)
    }
}

impl<'a, T: 'a> IntoIterator for crate::iter::OptionalDataToFlight<'a, T>
where
    T: FromU8Slice<'a> + Iterator
{
    type Item = Option<ProxyDataIterator<'a, T>>;
    type IntoIter = ProxyOptionalDataRowIterator<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        ProxyOptionalDataRowIterator::new(self.0)
    }
}