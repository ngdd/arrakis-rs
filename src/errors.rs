use std::error::Error;
use std::ffi;
use arrow::error::ArrowError;
use futures::channel::oneshot::Canceled;
use rdkafka::error::KafkaError;
use thiserror::Error as ThisError;
use tonic::Status;

#[derive(ThisError, Debug, Clone)]
pub enum ArrakisError {
    #[error("invalid producer url scheme")]
    BadUrl,
    #[error("blocks must only contain channels from a single source")]
    InvalidChannelSource,
    #[error("will not publish an empty block")]
    EmptyBlock,
    #[error("unexpected data layout returned from server")]
    InvalidSchemaReturned,

    #[error("invalid data stream returned, no schema found")]
    DataStreamHasNoSchema,
    #[error("unexpected end of data stream")]
    UnexpectedEndofDataStream,
    #[error("error parsing data stream")]
    ErrorParsingStream,
    #[error("no partition information returned from the server")]
    ErrorNoPartitionInfo,

    #[error("error connecting to kafka {0}")]
    ErrorOnKafkaConnection(String),
    #[error("error with publishing to kafka {0}")]
    ErrorOnKafkaPublish(String),

    #[error("Arrakis connection error {0}")]
    ErrorArrakisConnection(String),
    #[error("Arrakis flight error {0}")]
    ErrorArrakisFlight(String),
    #[error("Arrakis internal encoding error {0}")]
    ErrorArrakisInternalEncoding(String),
}

impl ArrakisError {
    pub(crate) fn connect_error<E>(e: E) -> Self
    where
        E: std::error::Error
    {
        Self::ErrorArrakisConnection(format!("{} - {1}", &e, e.to_string()))
    }
}

impl From<KafkaError> for ArrakisError {
    fn from(value: KafkaError) -> Self {
        let str_val = value.to_string();
        let src_val = format!("{:?}", value.source());
        let code_val = format!("{:?}", value.rdkafka_error_code());
        let msg = format!("Kafka Error str:({0}) src:({1}) code:({2})", str_val, src_val, code_val);
        println!("found kafka error: {0}", &msg);
        // let msg = match value {
        //     KafkaError::AdminOpCreation(s) => format!("Kafka AdminOpCreation({0}", s),
        //     KafkaError::AdminOp(ec) => format!("Kafka AdminOp({:?})", &ec),
        //     KafkaError::Canceled => "Kafka Canceled".to_string(),
        //     KafkaError::ClientConfig(res, v0, v1, v2) => format!("Kafka ClientConfig({:?}, {1}, {2}, {3})", &res, v0, v1, v2),
        //     KafkaError::ClientCreation(s) => format!("Kafka ClientCreation({0}", s),
        //     KafkaError::ConsumerCommit(ec) => format!("Kafka ConsumerCommit({:?}", &ec),
        //     KafkaError::ConsumerQueueClose(ec) => format!("Kafka ConsumerQueueClose({:?}", &ec),
        //     KafkaError::Flush(ec) => format!("Kafka Flush({:?}", &ec),
        //     KafkaError::Global(ec) => format!("Kafka Global({:?}", &ec),
        //     KafkaError::GroupListFetch(ec) => format!("Kafka GroupListFetch({:?}", &ec),
        //     KafkaError::MessageConsumption(ec) => format!("Kafka MessageConsumption({:?}", &ec),
        //     KafkaError::MessageConsumptionFatal(ec) => format!("Kafka MessageConsumptionFatal({:?}", &ec),
        //     KafkaError::MessageProduction(ec) => format!("Kafka MessageProduction({:?}", &ec),
        //     KafkaError::MetadataFetch(ec) => format!("Kafka MetadataFetch({:?}", &ec),
        //     KafkaError::NoMessageReceived => "Kafka NoMessageReceived".to_string(),
        //     KafkaError::Nul(_) => "Kafka Nul error".to_string(),
        //     KafkaError::OffsetFetch(ec) => format!("Kafka OffsetFetch({:?}", &ec),
        //     KafkaError::PartitionEOF(eof) => format!("Kafka PartitionEOF({0}", eof),
        //     KafkaError::PauseResume(s) => format!("Kafka PauseResume({0}", &s),
        //     KafkaError::Rebalance(ec) => format!("Kafka Rebalance({:?}", &ec),
        //     KafkaError::Seek(s) => format!("Kafka Seek({0}", &s),
        //     KafkaError::SetPartitionOffset(ec) => format!("Kafka SetPartitionOffset({:?}", &ec),
        //     KafkaError::StoreOffset(ec) => format!("Kafka StoreOffset({:?}", &ec),
        //     KafkaError::Subscription(s) => format!("Kafka Subscription({0}", &s),
        //     KafkaError::Transaction(ec) => format!("Kafka Transaction({:?}", &ec),
        //     KafkaError::MockCluster(ec) => format!("Kafka MockCluster({:?}", &ec),
        //
        //     _ => format!("General or Unknown kafka error: {} - {1} - {2}", &value, value.to_string()),
        // };
        Self::ErrorOnKafkaPublish(msg)
    }
}

impl From<Canceled> for ArrakisError {
    fn from(value: Canceled) -> Self {
        Self::ErrorOnKafkaPublish(format!("{} - {1}", &value, value.to_string()))
    }
}

impl From<tonic::Status> for ArrakisError {
    fn from(value: Status) -> Self {
        Self::ErrorArrakisFlight(format!("{} - {1}", &value, value.to_string()))
    }
}

impl From<ArrowError> for ArrakisError {
    fn from(value: ArrowError) -> Self {
        Self::ErrorArrakisFlight(format!("{} - {1}", &value, value.to_string()))
    }
}

impl From<serde_json::Error> for ArrakisError {
    fn from(value: serde_json::Error) -> Self {
        Self::ErrorArrakisFlight(format!("{} - {1}", &value, value.to_string()))
    }
}
