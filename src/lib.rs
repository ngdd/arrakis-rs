// Copyright (C) 2022, California Institute of Technology and contributors
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

pub mod publisher;
pub mod data;
pub mod iter;
pub mod errors;

use std::iter::FromIterator;
use std::any::Any;
use std::collections::{HashMap, HashSet};
use std::env;
use std::error::Error;
use std::io::Write;
use std::pin::Pin;
use std::sync::Arc;

use futures_util::pin_mut;

use arrow::{
    array::{
        as_string_array, MapArray, Int32Array, Int64Array, ArrayRef, StringArray,
        builder::{
            ListBuilder,
            Int16Builder,
            Int32Builder,
            Int64Builder,
            UInt32Builder,
            Float32Builder,
            Float64Builder,
        },
    },
    datatypes::{self, Field, Schema, SchemaRef},
    ipc::{self, reader},
    record_batch::RecordBatch,
};
use arrow::array::Array;
use arrow::ipc::writer::IpcWriteOptions;
//use arrow_flight::flight_service_client::FlightServiceClient;
use arrow_flight::{
    client::FlightClient,
    decode::FlightRecordBatchStream,
    encode::FlightDataEncoderBuilder,
    encode::FlightDataEncoder,
    utils::flight_data_to_arrow_batch,
    FlightData,
    FlightDescriptor,
};
use arrow_flight::utils::flight_data_from_arrow_batch;
use futures::TryStreamExt;
use futures::stream::{Stream, StreamExt};
use serde::{Deserialize, Serialize};
use tonic::{Response, Status, Streaming};
use crate::data::{PublishChannel, PartitionId, ChannelName};
use crate::errors::ArrakisError;
use crate::publisher::PartitionMapping;

//pub(crate) type FlightClient = FlightServiceClient<tonic::transport::Channel>;

pub static DEFAULT_ARRAKIS_SERVER: &str = "http://localhost:31206";

#[derive(Serialize, Deserialize, Debug)]
struct StreamRequest {
    channels: Vec<String>,
    start: Option<i32>,
    end: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct DescribeRequest {
    channels: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct MatchRequest {
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct PublishRequest {
    producer_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct PartitionRequest {
    producer_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "request", content = "args")]
enum Request {
    Stream(StreamRequest),
    Describe(DescribeRequest),
    Find(MatchRequest),
    Count(MatchRequest),
    Publish(PublishRequest),
    Partition(PartitionRequest),
}

/// Create a flight descriptor from a Request-like command
fn create_flight_descriptor(request: Request) -> serde_json::Result<FlightDescriptor> {
    // serialize request
    let cmd = serde_json::to_string(&request)?;

    // create a descriptor from cmd
    Ok(FlightDescriptor::new_cmd(cmd.into_bytes().to_vec()))
}

/// Send a request to an Arrow Flight compatible server
async fn send_flight_request(
    mut client: &mut FlightClient,
    request: Request,
) -> Result<FlightRecordBatchStream, ArrakisError> {
    let descriptor = create_flight_descriptor(request).expect("Failed to create descriptor");
    let flight = client.get_flight_info(descriptor);
    let flight_result = flight.await;

    let endpoints = flight_result.map_err(|e|ArrakisError::ErrorArrakisFlight(e.to_string()))?.endpoint;
    let ticket = endpoints[0].ticket.clone().unwrap();

    // send request to server, get response
    let response = client.do_get(ticket).await.map_err(|e|ArrakisError::ErrorArrakisFlight(e.to_string()))?;
    Ok(response)
}

/// Extract a flight schema from a response
async fn receive_flight_schema(response: &mut Streaming<FlightData>) -> Option<Arc<Schema>> {
    let data = response.message().await.ok()??;
    let message =
        arrow::ipc::root_as_message(&data.data_header[..]).expect("Error parsing message");

    // message header is a Schema, so read it
    let ipc_schema: ipc::Schema = message
        .header_as_schema()
        .expect("Unable to read IPC message as schema");
    let schema = ipc::convert::fb_to_schema(ipc_schema);

    Some(Arc::new(schema))
}

async fn to_record_batches(mut stream: FlightRecordBatchStream) -> Vec<RecordBatch> {
    let mut batches: Vec<RecordBatch> = Vec::new();

    while let Some(result) = stream.next().await {
        if let Ok(batch) = result {
            batches.push(batch)
        }
    };
    batches
}

fn decode_string_list(batches: &[RecordBatch]) -> Result<Vec<String>, ArrakisError> {
    let mut strings = Vec::<String>::new();
    // convert record batch into channel list
    for batch in batches {
        let array: &StringArray = batch.column(0).as_any().downcast_ref().ok_or(ArrakisError::ErrorArrakisInternalEncoding("Expected a string array".to_string()))?;
        strings.extend(array.iter().map(|s| s.unwrap().to_string()));
    }
    Ok(strings)
}

/// Connect to an Arrow Flight server
///
/// If the URL is not set, it will first check whether ARRAKIS_SERVER is set,
/// otherwise use the default server.
///
pub(crate) async fn connect(
    maybe_url: Option<String>,
) -> Result<FlightClient, ArrakisError> {
    let url = maybe_url.unwrap_or_else(|| env::var("ARRAKIS_SERVER").unwrap_or_else(|_| DEFAULT_ARRAKIS_SERVER.to_string()));
    let uri = url.parse::<tonic::transport::Uri>().map_err(|e| ArrakisError::BadUrl)?;
    let endpoint = tonic::transport::Channel::builder(uri);
    let channel = endpoint.connect().await.map_err(|e| ArrakisError::connect_error(e))?;
    Ok(FlightClient::new(channel))
}

/// Stream live or offline timeseries data
///
/// # Arguments
///
/// * `channels` - A list of channels to request
/// * `start` - The start time, if specified
/// * `end` - The end time, if specified
///
/// Setting neither `start` nor `end` starts a live stream
/// from now.
///
pub async fn stream(
    channels: Vec<String>,
    start: Option<i32>,
    end: Option<i32>,
) -> Result<FlightRecordBatchStream, ArrakisError> {
    // connect to client
    let mut client = connect(None).await?;

    // create and send request
    let request = Request::Stream(StreamRequest {
        channels,
        start,
        end,
    });
    let response = send_flight_request(&mut client, request).await?;
    Ok(response)
}

/// Get channel metadata for channels requested
///
/// # Arguments
///
/// * `channels` - A list of channels to request
///
pub async fn describe(
    channels: Vec<String>,
) -> Result<Vec<String>, ArrakisError> {
    // connect to client
    let mut client = connect(None).await?;

    // create and send request
    let request = Request::Describe(DescribeRequest {
        channels
    });
    let mut response = send_flight_request(&mut client, request).await?;

    let batches = to_record_batches(response).await;

    decode_string_list(batches.as_slice())
}


/// Find channels matching a set of conditions
///
/// # Arguments
///
/// * `pattern` - A channel pattern to match channels with
/// * `data_type` - Data types to match, if specified
/// * `min_rate` - Minimum sampling rate for channels, if specified
/// * `min_rate` - Maximum sampling rate for channels, if specified
///
pub async fn find(
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
) -> Result<Vec<String>, ArrakisError> {
    // connect to client
    let mut client = connect(None).await?;

    // create and send request
    let request = Request::Find(MatchRequest {
        pattern,
        data_type,
        min_rate,
        max_rate,
    });
    let mut response = send_flight_request(&mut client, request).await?;

    let batches = to_record_batches(response).await;

    // convert record batch into channel list
    decode_string_list(batches.as_slice())
}

/// Count channels matching a set of conditions
///
/// # Arguments
///
/// * `pattern` - A channel pattern to match channels with
/// * `data_type` - Data types to match, if specified
/// * `min_rate` - Minimum sampling rate for channels, if specified
/// * `min_rate` - Maximum sampling rate for channels, if specified
///
pub async fn count(
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
) -> Result<i64, ArrakisError> {
    // connect to client
    let mut client = connect(None).await?;

    // create and send request
    let request = Request::Count(MatchRequest {
        pattern,
        data_type,
        min_rate,
        max_rate,
    });
    let mut response = send_flight_request(&mut client, request).await?;

    let batches = to_record_batches(response).await;
    if let Some(batch) = batches.first() {
        // convert record batch into count
        let array = batch
            .column(0)
            .as_any()
            .downcast_ref::<Int64Array>().ok_or(ArrakisError::ErrorArrakisInternalEncoding("Expected a Int64Array".to_string()))?;
        return Ok(array.value(0));
    }
    Err(ArrakisError::ErrorArrakisInternalEncoding("No data returned".to_string()))
}

fn get_column(name: &str, batch: &RecordBatch) -> Option<ArrayRef> {
    let schema = batch.schema();
    let (index, _) = schema.fields().iter().enumerate().find(|(i, f)| {
        f.name().as_str() == name
    })?;
    Some(batch.column(index).clone())
}

fn get_str_key_value_map(data: ArrayRef) -> Option<HashMap<String, String>> {
    let map: &MapArray = data.as_any().downcast_ref().unwrap();
    let keys: &ArrayRef = map.keys();
    let keys: &StringArray = keys.as_any().downcast_ref().unwrap();
    let values: &ArrayRef = map.values();
    let values: &StringArray = values.as_any().downcast_ref().unwrap();
    let mut hash_map = HashMap::new();
    keys.iter().zip(values.iter()).for_each(|(k, v)| {
        if let Some(k) = k {
            if let Some(v) = v {
                hash_map.insert(k.to_string(), v.to_string());
            }
        }
    });
    Some(hash_map)
}

pub async fn register_publisher(producer_id: &str, source: &str, scope: &str) -> Result<publisher::Publisher, ArrakisError>{
    let mut client = connect(None).await?;

    let request = Request::Publish(PublishRequest{
        producer_id: producer_id.to_string(),
    });
    let mut response = send_flight_request(&mut client, request).await?;

    let batches = to_record_batches(response).await;

    let batch = batches.first().ok_or(ArrakisError::ErrorArrakisInternalEncoding("No data sent".to_string()))?;

    let properties_col = get_column("properties", &batch).ok_or(ArrakisError::InvalidSchemaReturned)?;

    let properties = get_str_key_value_map(properties_col).ok_or(ArrakisError::InvalidSchemaReturned)?;

    println!("Properties: {:?}", &properties);

    //let channels = find(Some(format!("{0}:{1}", source, scope)), None, None, None).await.expect("could not get channels");
    //println!("Found {0} channels in scope", channels.len());

    publisher::Publisher::new(client, &properties, producer_id.to_string(), source.to_string(), scope.to_string())
}

pub async fn register_bulk_publisher(producer_id: &str, source: &str) -> Result<publisher::BulkPublisher, ArrakisError> {
    let mut client = connect(None).await?;

    let request = Request::Publish(PublishRequest{
        producer_id: producer_id.to_string(),
    });
    let mut response = send_flight_request(&mut client, request).await?;

    let batches = to_record_batches(response).await;

    let batch = batches.first().ok_or(ArrakisError::ErrorArrakisInternalEncoding("No data sent".to_string()))?;

    let properties_col = get_column("properties", &batch).ok_or(ArrakisError::InvalidSchemaReturned)?;

    let properties = get_str_key_value_map(properties_col).ok_or(ArrakisError::InvalidSchemaReturned)?;

    println!("Properties: {:?}", &properties);

    //let channels = find(Some(format!("{0}:{1}", source, scope)), None, None, None).await.expect("could not get channels");
    //println!("Found {0} channels in scope", channels.len());

    Ok(publisher::BulkPublisher::new(client, &properties, producer_id.to_string(), source.to_string()))
}

pub(crate) async fn update_partitions(client: &mut FlightClient, producer_id: &str, channels: &HashSet<PublishChannel>) -> Result<Vec<PartitionMapping>, ArrakisError> {
    const CHUNK_SIZE:usize = 3000usize;
    println!("update_partitions called");
    let request = Request::Partition(PartitionRequest{
        producer_id:  producer_id.to_string(),
    });

    let descriptor = create_flight_descriptor(request).expect("Failed to create descriptor");

    let schema = Arc::new(Schema::new(vec![
        Field::new("channel", datatypes::DataType::Utf8, false),
        Field::new("data_type", datatypes::DataType::Utf8, false),
        Field::new("sample_rate", datatypes::DataType::Int32, false),
        Field::new("partition_id", datatypes::DataType::Utf8, true),
    ]));

    let mut records = Vec::new();

    let mut names_vec = Vec::new();
    let mut data_type_vec = Vec::new();
    let mut sample_rate_vec = Vec::new();
    let mut part_id : Vec<Option<String>> = Vec::new();
    for ( channel) in channels.iter() {
        names_vec.push(Some(channel.name()));
        data_type_vec.push(Some(channel.data_type.to_string()));
        sample_rate_vec.push(Some(channel.sample_rate));
        part_id.push(None);

        if names_vec.len() >= CHUNK_SIZE {
            let record = RecordBatch::try_new(schema.clone(), vec![
                Arc::new(StringArray::from_iter(names_vec.iter())),
                Arc::new(StringArray::from_iter(data_type_vec.iter())),
                Arc::new(Int32Array::from_iter(sample_rate_vec.iter())),
                Arc::new(StringArray::from_iter(part_id.iter())),
            ])?;
            records.push(Ok(record));

            names_vec.clear();
            data_type_vec.clear();
            sample_rate_vec.clear();
            part_id.clear();
        }
    }
    if !names_vec.is_empty() {
        let record = RecordBatch::try_new(schema.clone(), vec![
            Arc::new(StringArray::from_iter(names_vec.iter())),
            Arc::new(StringArray::from_iter(data_type_vec.iter())),
            Arc::new(Int32Array::from_iter(sample_rate_vec.iter())),
            Arc::new(StringArray::from_iter(part_id.iter())),
        ])?;
        records.push(Ok(record));

        names_vec.clear();
        data_type_vec.clear();
        sample_rate_vec.clear();
        part_id.clear();
    }

    let input_stream = FlightDataEncoderBuilder::new()
        .with_flight_descriptor(Some(descriptor))
        .build(futures::stream::iter(records));

    let response = client.do_exchange(input_stream).await;

    let mut response = response.map_err(|e|ArrakisError::ErrorArrakisFlight(e.to_string()))?;

    let mut results = Vec::new();
    results.reserve(channels.len());
    while let Some(batch) = response.next().await {
        let partition_info = match batch {
            Ok(record_batch) => record_batch,
            Err(_) => continue,
        };
        let channel_col = get_column("channel", &partition_info).ok_or(ArrakisError::InvalidSchemaReturned)?;
        let channel_col = channel_col.as_any().downcast_ref::<StringArray>().ok_or(ArrakisError::InvalidSchemaReturned)?;
        let partition_id_col = get_column("partition_id", &partition_info).ok_or(ArrakisError::InvalidSchemaReturned)?;
        let partition_id_col = partition_id_col.as_any().downcast_ref::<StringArray>().ok_or(ArrakisError::InvalidSchemaReturned)?;

        channel_col.iter().zip(partition_id_col.iter()).for_each(|(name, partition)| {
            if let Some(name) = name {
                if let Some(partition) = partition {
                    results.push(PartitionMapping{
                        channel_name: ChannelName(name.to_string()),
                        partition_id: PartitionId(partition.to_string())
                    });
                }
            }
        });

    }
    println!("update_partitions done");
    Ok(results)
}