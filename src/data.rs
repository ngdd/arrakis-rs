use std::cmp::{Eq, PartialEq};
use std::convert::{From, TryFrom};
use std::collections::hash_map::Values;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::fmt::Display;
use std::error::Error;
use serde::{Serialize, Serializer};

use thiserror::Error as ThisError;

#[derive(ThisError, Debug)]
pub enum DataError {
    #[error("invalid string format")]
    InvalidString,
    #[error("data type and sample rate must be specified to build a publishing channel")]
    PublishChannelMissingFields,
}

#[derive(Clone,Debug,PartialEq,Eq,Hash)]
pub enum DataType {
    Int16,
    Int32,
    Int64,
    Float32,
    Float64,
    UInt32,
}

impl DataType {
    pub(crate) fn data_size(&self) -> usize {
        match self {
            DataType::Int16 => std::mem::size_of::<i16>(),
            DataType::Int32 => std::mem::size_of::<i32>(),
            DataType::Int64 => std::mem::size_of::<i64>(),
            DataType::Float32 => std::mem::size_of::<f32>(),
            DataType::Float64 => std::mem::size_of::<f64>(),
            DataType::UInt32 => std::mem::size_of::<u32>(),
        }
    }
}

impl Display for DataType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            DataType::Int16 => "int16",
            DataType::Int32 => "int32",
            DataType::Int64 => "int64",
            DataType::Float32 => "float32",
            DataType::Float64 => "float64",
            DataType::UInt32 => "uint32",
        })
    }
}

impl serde::Serialize for DataType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.collect_str(self)
    }
}

// impl PartialEq for DataType {
//     fn eq(&self, other: &Self) -> bool {
//         return *self == *other
//     }
// }

pub struct DataTypeIterator {
    val: Option<DataType>,
}

impl DataTypeIterator {
    pub fn new() -> Self {
        Self{
            val: Some(DataType::Int16),
        }
    }
}

impl Iterator for DataTypeIterator {
    type Item = DataType;

    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.val.clone();
        self.val = match self.val {
            Some(DataType::Int16) => Some(DataType::Int32),
            Some(DataType::Int32) => Some(DataType::Int64),
            Some(DataType::Int64) => Some(DataType::Float32),
            Some(DataType::Float32) => Some(DataType::Float64),
            Some(DataType::Float64) => Some(DataType::UInt32),
            Some(DataType::UInt32) => None,
            None => None,
        };
        ret
    }
}

#[derive(Debug,Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
pub(crate) struct ChannelName(pub String);
#[derive(Debug,Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
pub(crate) struct PartitionId(pub String);

#[derive(Clone,Debug,Eq,Hash)]
pub struct Channel {
    pub source: String,
    pub identifier: String,
    pub time: Option<i64>,
    pub data_type: Option<DataType>,
    pub sample_rate: Option<i32>,
}

impl PartialEq for Channel {
    fn eq(&self, other: &Self) -> bool {
        if self.source != other.source ||
            self.identifier != other.identifier
        {
            return false;
        }
        if self.data_type != other.data_type {
            return false;
        }
        if self.sample_rate != other.sample_rate {
            return false;
        }
        true
    }
}

impl Display for Channel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}:{1}", self.source, self.identifier)
    }
}

impl From<PublishChannel> for Channel {
    fn from(value: PublishChannel) -> Self {
        Self{
            source: value.source,
            identifier: value.identifier,
            time: None,
            data_type: Some(value.data_type),
            sample_rate: Some(value.sample_rate),
        }
    }
}

impl Channel {
    pub fn new_from_string(name: &str) -> Result<Self, Box<dyn Error>> {
        let split_index = name.find(":").ok_or(DataError::InvalidString)?;
        let source = (&name[0..split_index]).to_string();
        let identifier = (&name[split_index+1..]).to_string();

        Ok(Self{
            source,
            identifier,
            time: None,
            data_type: None,
            sample_rate: None,
        })
    }
    pub fn new(name: &str, time: Option<i64>, data_type: Option<DataType>, sample_rate:Option<i32>) -> Result<Self, Box<dyn Error>> {
        let mut chan = Self::new_from_string(name)?;
        chan.time = time;
        chan.data_type = data_type;
        chan.sample_rate = sample_rate;
        Ok(chan)
    }
    pub fn name(&self) -> String {
        format!("{0}", self)
    }

    pub fn to_json() {
        todo!();
    }
}

#[derive(Clone,Debug,Eq,Hash,Serialize)]
pub struct PublishChannel {
    pub source: String,
    pub identifier: String,
    pub data_type: DataType,
    pub sample_rate: i32,
}

impl PublishChannel {
    pub fn new(name: &str, data_type: DataType, sample_rate: i32) -> Result<Self, Box<dyn Error>> {
        let split_index = name.find(":").ok_or(DataError::InvalidString)?;
        let source = (&name[0..split_index]).to_string();
        let identifier = (&name[split_index+1..]).to_string();
        Ok(Self{
            source,
            identifier,
            data_type,
            sample_rate,
        })
    }
    pub fn name(&self) -> String {
        format!("{0}", self)
    }

    pub(crate) fn channel_name(&self) -> ChannelName {
        ChannelName(self.name())
    }

    pub fn to_json() {
        todo!();
    }
}

impl TryFrom<Channel> for PublishChannel {
    type Error = DataError;

    fn try_from(value: Channel) -> Result<Self, Self::Error> {
        let data_type = value.data_type.ok_or(DataError::PublishChannelMissingFields)?;
        let sample_rate = value.sample_rate.ok_or(DataError::PublishChannelMissingFields)?;
        Ok(Self{
            source: value.source,
            identifier: value.identifier,
            data_type,
            sample_rate,
        })
    }
}

impl Display for PublishChannel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}:{1}", self.source, self.identifier)
    }
}

impl PartialEq for PublishChannel {
    fn eq(&self, other: &Self) -> bool {
        if self.source != other.source ||
            self.identifier != other.identifier ||
            self.data_type != other.data_type ||
            self.sample_rate != other.sample_rate
        {
            return false;
        }
        true
    }
}

pub trait SeriesBackingStore {
    type Item;

    fn data_item_to_slice<'a>(&'a self, i: &'a Self::Item) -> &'a [u8];
}

pub struct VecBackingStore {}

impl SeriesBackingStore for VecBackingStore {
    type Item = Vec<u8>;

    fn data_item_to_slice<'a>(&'a self, i: &'a Self::Item) -> &'a [u8] {
        i.as_slice()
    }
}

#[derive(Debug)]
pub struct BasicSeriesBlock<S: SeriesBackingStore> {
    pub time: u64,
    pub data: HashMap<String, S::Item>,
    pub duration: f64,
    pub metadata: HashMap<String, PublishChannel>,
    pub backing_store: S,
}

impl<S: SeriesBackingStore> BasicSeriesBlock<S> {
    pub fn channels(&self) -> Values<'_, String, PublishChannel> {
        self.metadata.values()
    }

    pub fn channel(&self, channel_name: &str) -> Option<&PublishChannel> {
        self.metadata.get(channel_name)
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }
}


pub type SeriesBlock = BasicSeriesBlock<VecBackingStore>;

impl SeriesBlock {
    pub fn new(timestamp: u64) -> Self {
        Self{
            time: timestamp,
            data: HashMap::new(),
            duration: 0.0,
            metadata: HashMap::new(),
            backing_store: VecBackingStore{},
        }
    }
}